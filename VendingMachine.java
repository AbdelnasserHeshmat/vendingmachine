
package vendingmachine;

/**
 * @author Abdelnasser Heshmat
 *  BN : 27   SEC: 1
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.*; 


/**
 * Sample implementation of Vending Machine in Java
 *
 * @author Javin Paul
 */
public class VendingMachine {

    private Inventory<Money> cashInventory = new Inventory<Money>();
    private Inventory<Item> itemInventory = new Inventory<Item>();
    LinkedList<Bill> BillList = new LinkedList<Bill>();
    private long currentPayment;
    private long totalCost;

    public VendingMachine() {
        totalCost=0;
        currentPayment=0;
        for (Money m : Money.values()) {
            cashInventory.put(m , 7);    //put 5 items in evey money type
        }
        for (Item i : Item.values()) {
            itemInventory.put(i, 7);
        }
    }
    
    public void DisplayAvailable(Item I){

        System.out.println( itemInventory.getQuantity(I)+ " --> available of "+ I.getName()); 
    }
    
    public void selectProduct(Item I, int ItemQuantity){
        BillList.add(new Bill(ItemQuantity,I));
    }
    
    public void insertMoney(Money M){
        this.currentPayment = this.currentPayment + M.getDenomination();
        cashInventory.add(M);
    }
    
    public long getBillTotalCost(){
        totalCost=0;
        for( int i=0;i< BillList.size();i++){
          //  System.out.println( i);
           totalCost =totalCost+ BillList.get(i).getCost();
        }
        return totalCost;
    }
    
    public boolean hasEnoughItems(Item I, int Quantity){
        if(itemInventory.getQuantity(I)>= Quantity){
            return true;
        }
        try {
            throw new SoldOutException("Sold Out, Please buy another item");
        } catch (SoldOutException e) {
            System.out.println("Sold Out, Please buy another item");
        }
        
        return false;
    }
    
    public boolean IsPaymentEnough(){
      if( getBillTotalCost()<=currentPayment)
      {
          
          return true;
      }
        try {
            throw new NotSufficientPaidException("Price not full paid. Please add more money, remaining : ", getBillTotalCost() - currentPayment);
        } catch (NotSufficientPaidException e) {
            System.out.println("Price not full paid. Please add more money, remaining : " + (getBillTotalCost() - currentPayment));
        }
        return false;
    }
    
    public List<Money> getChange(long amount) throws NoChangeAvailableException {
        List<Money> changes = Collections.EMPTY_LIST;
        if (IsPaymentEnough()) {
            if (amount > 0) {
                changes = new ArrayList<Money>();
                long balance = amount;
                while (balance > 0) {
                    if (balance >= Money.Dollar2.getDenomination() && cashInventory.hasItem(Money.Dollar2)) {
                        changes.add(Money.Dollar2);
                        balance = balance - Money.Dollar2.getDenomination();
                        continue;

                    } else if (balance >= Money.Dollar1.getDenomination() && cashInventory.hasItem(Money.Dollar1)) {
                        changes.add(Money.Dollar1);
                        balance = balance - Money.Dollar1.getDenomination();
                        continue;

                    } else if (balance >= Money.QUARTER.getDenomination() && cashInventory.hasItem(Money.QUARTER)) {
                        changes.add(Money.QUARTER);
                        balance = balance - Money.QUARTER.getDenomination();
                        continue;

                    } else if (balance >= Money.DIME.getDenomination() && cashInventory.hasItem(Money.DIME)) {
                        changes.add(Money.DIME);
                        balance = balance - Money.DIME.getDenomination();
                        continue;

                    } else if (balance >= Money.NICKLE.getDenomination() && cashInventory.hasItem(Money.NICKLE)) {
                        changes.add(Money.NICKLE);
                        balance = balance - Money.NICKLE.getDenomination();
                        continue;

                    } else if (balance >= Money.PENNY.getDenomination() && cashInventory.hasItem(Money.PENNY)) {
                        changes.add(Money.PENNY);
                        balance = balance - Money.PENNY.getDenomination();
                        continue;

                    } else {
                        try {
                            throw new NoChangeAvailableException("NotSufficientChange");
                        } catch (NotSufficientPaidException e) {
                            System.out.println("NotSufficientChange");
                        }

                    }
                }
            }
        }
        return changes;
    }
    
    public void refund(){
        
       System.out.println( "Your request is canceled and  "
               + currentPayment + " cents get back to you");
        List<Money> refund = getChange(currentPayment);
        updateCashInventory(refund);
      currentPayment=0; 
        
    }
    
    private void updateCashInventory(List<Money> change) {
        for (int i =0 ; i < change.size();i++) {
            
            cashInventory.deduct(change.get(i));
        }
    }

    
    public void CompleteRequest(){
        
        if (IsPaymentEnough()){
            List<Money> change = getChange( currentPayment-getBillTotalCost());
            updateCashInventory(change);
            
            
            System.out.println("\n************ Bill ***************");

           // deduct bill items and print
            for (int i =0 ; i < BillList.size();i++) {
               
               System.out.println( (i+1) + " | "+ BillList.get(i).getQuantity() +
                     " of " +  BillList.get(i).getItem() +
                     "  cost: "+ BillList.get(i).getCost()+ " cents");
                
            for(int j =0 ; j < BillList.get(i).getQuantity() ;j++){
            itemInventory.deduct(BillList.get(i).getItem());
            }
        }
            System.out.println("   total cost   = " + getBillTotalCost()+" cents");
            System.out.println("current Payment = " + currentPayment +" cents");
            System.out.println("**********************************");
            
            System.out.println("change = " + (currentPayment-getBillTotalCost()+"\n"));
           BillList.clear();
           currentPayment=0;
           
            
        }
    }
    }
    

enum Money {
    PENNY(1), NICKLE(5), DIME(10), QUARTER(25),halfDollar(50), Dollar1(100), Dollar2(200);

    private int denomination;

    private Money(int denomination) {
        this.denomination = denomination;
    }

    public int getDenomination() {
        return denomination;
    }
}

enum Item {
    CANDY("CANDY", 10), SNACK("SNACK", 50), NUTS("NUTS", 90)
    , Coke("Coke", 25), Pepsi("Pepsi", 35), Soda("Soda", 45);
    private String name;
    private int price;

    private Item(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }
}


class Inventory<T> {

    private Map<T, Integer> inventory = new HashMap<T, Integer>();

    public int getQuantity(T item) {
        Integer value = inventory.get(item);
        return value == null ? 0 : value;
    }

    public void add(T item) {
        int count = inventory.get(item);
        inventory.put(item, count + 1);
    }

    public void deduct(T item) {
        if (hasItem(item)) {
            int count = inventory.get(item);
            inventory.put(item, count - 1);
        }
    }

    public boolean hasItem(T item) {
        return getQuantity(item) > 0;
    }

    public void clear() {
        inventory.clear();
    }

    public void put(T item, int quantity) {
        inventory.put(item, quantity);
    }
}

class Bill {

    private Item I;
    private long C;
    private int N;

    public Bill( int N, Item I) {
        this.I = I;
        this.C = I.getPrice();
        this.N = N;
    }

    public long getCost() {
        return (C*N);
    }
    public Item getItem() {
        return I;
    }
    public int getQuantity() {
        return N;
    }

}

class NotSufficientPaidException extends RuntimeException {

    private String message;
    private long remaining;

    public NotSufficientPaidException(String message, long remaining) {
        this.message = message;
        this.remaining = remaining;
    }

    public long getRemaining() {
        return remaining;
    }

    @Override
    public String getMessage() {
        return message + remaining;
    }
}

class NoChangeAvailableException extends RuntimeException {

    private String message;

    public NoChangeAvailableException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

class SoldOutException extends RuntimeException {

    private String message;

    public SoldOutException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }

}


