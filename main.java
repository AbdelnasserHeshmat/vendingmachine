
package vendingmachine;

/**
 * @author Abdelnasser Heshmat
 *  BN : 27   SEC: 1
 */
public class main {
    
    public static void main(String[] args) {
    
        
        VendingMachine VM = new VendingMachine();
        
        //Display the quantity of Nuts & Soda before transaction:
        VM.DisplayAvailable(Item.NUTS);  
        VM.DisplayAvailable(Item.Soda); 
        
        // products selection:
        VM.selectProduct(Item.NUTS, 1);  
        VM.selectProduct(Item.Coke, 2);
        VM.selectProduct(Item.SNACK, 1);
        VM.selectProduct(Item.Pepsi, 1);
        VM.selectProduct(Item.Soda, 3);
        
        // inserting Money in different types:
        VM.insertMoney(Money.Dollar2 );        
        VM.insertMoney(Money.Dollar1);
        VM.insertMoney(Money.QUARTER );
        VM.insertMoney(Money.NICKLE );
        VM.insertMoney(Money.halfDollar );
        
        // checking if Money inserted is enough:
        if (VM.IsPaymentEnough())
            System.out.println( "Payment is Eonugh");
        
        //VM.refund();
        
        // submit request and printing Bill :
        VM.CompleteRequest();
        
        //Display the quantity of Nuts & Soda after transaction
        VM.DisplayAvailable(Item.NUTS);
        VM.DisplayAvailable(Item.Soda);
 
    }
    
}
